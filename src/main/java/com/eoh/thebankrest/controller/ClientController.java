package com.eoh.thebankrest.controller;

import com.eoh.thebankrest.entity.Client;
import com.eoh.thebankrest.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/clients")
public class ClientController {
    @Autowired
    private ClientService clientService;


    @GetMapping("/all")
    public List<Client> getAll()
    {
        return clientService.getAllClients();
    }

    @PostMapping("/login")
    public Client getClientByName(@RequestBody Client client){
        String name = client.getName();

        return clientService.getClientByName(name);
    }

    @GetMapping("get-client/{client_id}")
    public Client getClientById(@PathVariable Long client_id){
        return clientService.getClientById(client_id);
    }

}
