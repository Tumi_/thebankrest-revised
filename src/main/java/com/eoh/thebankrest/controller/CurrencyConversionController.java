package com.eoh.thebankrest.controller;

import com.eoh.thebankrest.entity.Currency_conversion_rate;
import com.eoh.thebankrest.service.CurrencyConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class CurrencyConversionController {

    @Autowired
    private CurrencyConversionService currencyConversionService;

    @GetMapping("/currency-rate/{currency_code}")
    public Currency_conversion_rate getConversion(@PathVariable String currency_code){
        return currencyConversionService.getConversionRateByCode(currency_code);
    }
}
