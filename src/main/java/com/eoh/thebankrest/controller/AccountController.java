package com.eoh.thebankrest.controller;

import com.eoh.thebankrest.entity.Client;
import com.eoh.thebankrest.entity.Client_account;
import com.eoh.thebankrest.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/account")
@CrossOrigin("*")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/transactional/{client_id}")
    public List<Client_account> getTransactional(@PathVariable Long client_id){
        return accountService.getTransactionalAccounts(client_id);
    }

    @GetMapping("/currency-accounts/{client_id}")
    public List<Client_account> getCurrency(@PathVariable Long client_id){
        return accountService.getUserCurrencyAccounts(client_id);
    }

    @GetMapping("/converted/{currency_code}/{display_balance}/{rate}")
    public Double convertCurrency(@PathVariable String currency_code, @PathVariable Double display_balance,@PathVariable Double rate){
        return accountService.convertedCurrency(currency_code,display_balance,rate);
    }

    @PutMapping("/transactional/withdraw/{client_account_number}")
    public int withdraw(@PathVariable Long client_account_number, @RequestBody Client_account account){
        double amount = account.getDisplay_balance();
        return  accountService.withdraw(client_account_number,amount);
    }

   @GetMapping("/get-account/{client_account_number}")
   public Client_account getAccountByNumber(@PathVariable Long client_account_number){
        return accountService.getAccByAccountNumber(client_account_number);
   }

   @GetMapping("/get-all-accounts/{client_id}")
    public List<Client_account> getAllUserAccounts(@PathVariable Long client_id){
        return accountService.getAllUserAccounts(client_id);
   }

   @GetMapping("/highest/{client_id}")
    public Double getHighestUserAccountBalance(@PathVariable Long client_id){
        return accountService.getHighestUserAccountBal(client_id);
   }

   @GetMapping("/financial-position/{client_id}")
    public Double getUserFinancialPosition(@PathVariable Long client_id){
        return accountService.getFinancialPosition(client_id);
   }


}
