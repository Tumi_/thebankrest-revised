package com.eoh.thebankrest.repository;

import com.eoh.thebankrest.entity.Account_Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountTypeRepository extends JpaRepository<Account_Type, String> {
    //Select Transactional Accounts
    @Query("SELECT ac from Account_Type ac WHERE ac.transactional = ?1")
    List<Account_Type> getAllByTransactional(int transactional);
}
