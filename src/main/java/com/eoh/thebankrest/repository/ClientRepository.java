package com.eoh.thebankrest.repository;

import com.eoh.thebankrest.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("select c from Client c where c.name = ?1")
    Client getClientByName(String name);

    @Query("select c from Client c where  c.client_id = ?1")
    Client getClientById(Long id);

}
