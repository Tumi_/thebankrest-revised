package com.eoh.thebankrest.repository;

import com.eoh.thebankrest.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, String> {

    @Query("select c from Currency c WHERE c.currency_code=?1")
    Currency getCurrencyByCode(String currency_code);
}
