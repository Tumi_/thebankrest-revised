package com.eoh.thebankrest.repository;

import com.eoh.thebankrest.entity.Currency_conversion_rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyConversionRepository extends JpaRepository<Currency_conversion_rate, String> {

    @Query("select c from Currency_conversion_rate c WHERE c.currency_code=?1")
    Currency_conversion_rate getConversionRateByCode(String currency_code);
}
