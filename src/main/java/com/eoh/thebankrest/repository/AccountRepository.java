package com.eoh.thebankrest.repository;

import com.eoh.thebankrest.entity.Client_account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Client_account, Long> {
    //Select transactional accounts only - should be modified
    @Query("select ca FROM Client_account ca where ca.client_id=?1 And " +
            "(ca.account_type_code='SVGS' OR ca.account_type_code='CCRD' OR ca.account_type_code='CHQ')" +
            " ORDER BY ca.display_balance DESC")
    List<Client_account> getUserTransactional(Long client_id);

    //Select currency accounts
    @Query("select a from Client_account a where a.client_id=?1 and a.account_type_code='CFCA'")
    List<Client_account> getUserCurrencyAcc(Long client_id);

    //Select account by account-number
    @Query("select a from Client_account a where a.client_account_number = ?1")
    Client_account getClient_accountByClient_account_number(Long accountNum);

    //Withdrawing query
    @Modifying
    @Transactional
    @Query(value = "update Client_account a SET a.display_balance=?1 WHERE a.client_account_number=?2", nativeQuery = true)
    int update(double display_balance, Long client_account_number);

    //Select all accounts by client id
    @Query("select a from Client_account a where a.client_id = ?1")
    List<Client_account> getAlUserAccounts(Long client_di);

    //Get Highest User Account Balance
    @Query("select MAX(a.display_balance) from Client_account a where a.client_id =?1")
    Double getHighestUserAccountBal(Long client_id);

    //Get sum of User loan accounts ()
    @Query("select SUM(a.display_balance) from Client_account a where a.client_id=?1" +
            " AND a.account_type_code In('PLOAN','HLOAN')")
    Double getSumOfUserLoanAccounts(Long client_id);

    //Get sum of transactional accounts
    @Query("select SUM(a.display_balance) from Client_account a where a.client_id=?1 " +
            "AND a.account_type_code NOT In('PLOAN','HLOAN', 'CFCA')")
    Double getSumOfUserTransactionalAccounts(Long client_id);
}

