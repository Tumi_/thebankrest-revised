package com.eoh.thebankrest.service;

import com.eoh.thebankrest.entity.Client;
import com.eoh.thebankrest.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public List<Client> getAllClients()
    {
        return clientRepository.findAll();
    }

    public Client getClientByName(String name){
        return clientRepository.getClientByName(name);
    }

    public Client getClientById(Long id){ return clientRepository.getClientById(id);}
}
