package com.eoh.thebankrest.service;

import com.eoh.thebankrest.entity.Currency;
import com.eoh.thebankrest.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyService {

    @Autowired
    private final CurrencyRepository currencyRepository;

    public CurrencyService(CurrencyRepository currencyRepository){
        this.currencyRepository = currencyRepository;
    }

    public List<Currency> getAll(){
        return currencyRepository.findAll();
    }

    public Currency getCurrencyByCode(String currency_code){
        return currencyRepository.getCurrencyByCode(currency_code);
    }

}
