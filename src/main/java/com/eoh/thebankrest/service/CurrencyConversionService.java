package com.eoh.thebankrest.service;

import com.eoh.thebankrest.entity.Currency_conversion_rate;
import com.eoh.thebankrest.repository.CurrencyConversionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyConversionService {

    @Autowired
    private CurrencyConversionRepository currencyConversionRepository;

    public Currency_conversion_rate getConversionRateByCode(String currency_code){
        return currencyConversionRepository.getConversionRateByCode(currency_code);
    }
}
