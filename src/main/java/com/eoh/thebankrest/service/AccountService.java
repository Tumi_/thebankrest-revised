package com.eoh.thebankrest.service;

import com.eoh.thebankrest.entity.Client_account;
import com.eoh.thebankrest.entity.Currency_conversion_rate;
import com.eoh.thebankrest.repository.AccountRepository;
import com.eoh.thebankrest.repository.AccountTypeRepository;
import com.eoh.thebankrest.repository.CurrencyConversionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountTypeRepository accountTypeRepository;

    @Autowired
    private CurrencyConversionRepository currencyConversionRepository;

    public List<Client_account> getTransactionalAccounts(Long clientid) {
        return accountRepository.getUserTransactional(clientid);
    }

    public List<Client_account> getUserCurrencyAccounts(Long clientid) {
        return accountRepository.getUserCurrencyAcc(clientid);
    }

    public Currency_conversion_rate getConversionRateByCurrency(String currency) {
        return currencyConversionRepository.getConversionRateByCode(currency);
    }

    public Double convertedCurrency(String currency, double display_balance, double rate) {
        Currency_conversion_rate currency_conversion_rate = getConversionRateByCurrency(currency);

        double zarAmnt = 0;

        if (currency_conversion_rate.getConversion_indicator().equals("/")) {
            zarAmnt = display_balance / rate;
        } else if (currency_conversion_rate.getConversion_indicator().equals("*")) {
            zarAmnt = display_balance * rate;
        }
        return zarAmnt;
    }

    public Client_account getAccByAccountNumber(Long accNumber) {
        return accountRepository.getClient_accountByClient_account_number(accNumber);
    }

    public int withdraw(Long accountNum, Double amount) {
        Client_account client_account = accountRepository.getClient_accountByClient_account_number(accountNum);

        double newBalance = 0;

        if (client_account.getDisplay_balance() > amount) {
            newBalance = client_account.getDisplay_balance() - amount;
        } else if (client_account.getAccount_type_code().equals("CHQ") && (client_account.getDisplay_balance() > -10000)) {
            newBalance = client_account.getDisplay_balance() - amount;
        } else {
            throw new IllegalArgumentException("Transaction cannot be done, insufficient funds");
        }
        return accountRepository.update(newBalance, accountNum);
    }

    public List<Client_account> getAllUserAccounts(Long client_id){
        List<Client_account> allAccounts = accountRepository.getAlUserAccounts(client_id);

        if (allAccounts == null){
            throw new IllegalArgumentException("This user has no accounts");
        } else {
            return allAccounts;
        }
    }

    public Double getHighestUserAccountBal(Long client_id){
        return accountRepository.getHighestUserAccountBal(client_id);
    }

    public Double getFinancialPosition(Long client_id){
        double loan = 0;
        double transactional = 0;
        double position = 0;

        loan = accountRepository.getSumOfUserLoanAccounts(client_id);

        //Since all loan balances are already negative
        position = transactional + loan;

        return position;
    }
}
