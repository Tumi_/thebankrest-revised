package com.eoh.thebankrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThebankRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThebankRestApplication.class, args);
    }

}
