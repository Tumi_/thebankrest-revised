package com.eoh.thebankrest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Denomination implements Serializable {

    @Id
    private Long denomination_id;

    private double value;
    private String denomination_type_code;

}
