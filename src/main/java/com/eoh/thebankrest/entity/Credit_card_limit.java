package com.eoh.thebankrest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Credit_card_limit implements Serializable {

    @Id
    private String client_account_number;

    private double account_limit;
}
