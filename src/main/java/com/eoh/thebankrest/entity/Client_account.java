package com.eoh.thebankrest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Client_account implements Serializable {

    @Id
    private Long client_account_number;

    private long client_id;
    private String account_type_code;
    private String currency_code;
    private double display_balance;
}
