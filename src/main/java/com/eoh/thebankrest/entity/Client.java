package com.eoh.thebankrest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Client implements Serializable {

    @Id
    private Long client_id;

    private String title;
    private String name;
    private String surname;
    private Date dob;
    private String client_sub_type_code;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "client_sub_type_code")
//    @JsonIgnore
//    private Client_sub_type client_sub_type;
}


