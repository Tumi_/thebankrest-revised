package com.eoh.thebankrest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Account_Type implements Serializable {

    @Id
    private String account_type_code;
    private String description;
    private int transactional;

}
