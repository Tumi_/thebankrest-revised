# theBankrest-revised

###PREREQUISITES:

- **Java 8 or Newer**
- **Your preferred IDE**

####STEPS:

- Clone the repo
- Import the pom.xml into your IDE
- Run the App
- Visit http://localhost:8080 in your browser